const { GoogleSpreadsheet } = require('google-spreadsheet');
const creds = require('./key.json');

class GoogleSheet {
    constructor(sheetId = '1s_fU_PIi3nwTWBGNsLvmVt-dc0W147MnlCI4BKdDek0') {
        this.doc = new GoogleSpreadsheet(sheetId);
    }

    async addRow(sheetTitle = 'Error', row = {}) {
        await this.doc.useServiceAccountAuth(creds);
        await this.doc.loadInfo();
        const successSheet = this.doc.sheetsByTitle[sheetTitle];
        return await successSheet.addRow(row);
    }

}

module.exports = GoogleSheet;
